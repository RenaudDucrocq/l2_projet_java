package ulco.cardGame.common.games.components;

public class Card extends Component{
    protected boolean hidden;

    public Card(String name, Integer value,boolean hidden) {
        super(name, value);
        this.setHidden(hidden);
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public String toString() {
        return "Card{" +
                "name=" + name +
                '}';
    }
}
